<?php

use PHPUnit\Framework\TestCase;
use TDD\Receipt;

require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

class ReceiptTest extends TestCase
{
    public function setUp()
    {
        // Create the object or other values to test the object
        $this->Receipt = new Receipt();
    }

    public function tearDown()
    {
        // Clean up loose ends after testing if there are any
        unset($this->Receipt);
    }

    /**
     * @dataProvider provideTotal
     */
    public function testTotal($items, $expected)
    {
        // No coupon
        $coupon = null;

        // Result of the $items fed to total method in Receipt class
        $output = $this->Receipt->total($items, $coupon);

        $this->assertEquals(
            $expected,
            $output,

            // Test did not pass
            "When summing the total should equal {$expected}"
        );

    }

    // Provide data
    public function provideTotal()
    {
        // List of different values which are used in testTotal method,
        // first item is an array of items to be summed, second is expected value for the sum
        return [
            // Categorize data with same expected totals
            'ints totaling 16' => [[1, 2, 5, 8], 16],
            [[-1, 2, 5, 8], 14],
            [[1, 2, 8], 11],
        ];
    }

    public function testTotalAndCoupon()
    {
        // Values to be tested and fed to total method in Receipt class
        $input = [0, 2, 5, 8];

        // With coupon
        $coupon = 0.20;

        $output = $this->Receipt->total($input, $coupon);
        $this->assertEquals(
            12,
            $output,

            // Test did not pass
            'When summing the total should equal 12'
        );
    }

    public function testPostTaxTotal()
    {
        $items = [1, 2, 5, 8];
        $tax = 0.20;
        $coupon = null;

        // Create a customized test double, using custom requirements
        $Receipt = $this->getMockBuilder('TDD\Receipt')
            // Test tax and total methods at the same time
            ->setMethods(['tax', 'total'])
            // Create mock class containing only methods above
            ->getMock();

        // Configure stub, set up expectation for method total to be called once with $items and $coupon as its parameters and return 10.00
        $Receipt->expects($this->once())
            ->method('total')
            ->with($items, $coupon)
            ->will($this->returnValue(10.00));

        // Configure stub, set up expectation for method tax to be called once with 10.00 and $tax as its parameters and return 1.00
        $Receipt->expects($this->once())
            ->method('tax')
            ->with(10.00, $tax)
            ->will($this->returnValue(1.00));

        // Test total and tax method at the same time; an array, tax and coupon arguments
        $result = $Receipt->postTaxTotal([1, 2, 5, 8], 0.20, null);

        // Check if total after calculating tax without coupon is correct
        $this->assertEquals(
            11.00,
            $result
        );
    }

    public function testTax()
    {
        // Values to be tested with new tax method
        $inputAmount = 10.00;
        $taxInput = 0.10;

        // Result using 2 variables above with tax method in Receipt class
        $output = $this->Receipt->tax($inputAmount, $taxInput);

        $this->assertEquals(
            1.00,
            $output,

            // Test did not pass
            'The tax calculation should equal 1.00'
        );
    }
}